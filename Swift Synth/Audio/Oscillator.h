//
//  Oscillator.h
//  Swift Synth
//
//  Created by Elena Dobosariu on 5/27/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef CGFloat (^Signal)(CGFloat);

typedef NS_ENUM(NSInteger, Waveform) {
    sine,
    triangle,
    sawtooth,
    square,
    whiteNoise
};

@interface Oscillator: NSObject

@property (assign, class) CGFloat amplitude;
@property (assign, class) CGFloat frequency;

@property (nonnull, class, readonly) Signal sine;
@property (nonnull, class, readonly) Signal whiteNoise;
@property (nonnull, class, readonly) Signal square;
@property (nonnull, class, readonly) Signal sawtooth;
@property (nonnull, class, readonly) Signal triangle;


@end
