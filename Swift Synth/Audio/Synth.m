//
//  Synth.m
//  Swift Synth
//
//  Created by Elena Dobosariu on 5/27/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Synth.h"
#import <AVFoundation/AVFoundation.h>

@interface Synth ()

@property (nonatomic, strong) AVAudioEngine *audioEngine;
@property (nonatomic, assign) CGFloat time;
@property (nonatomic, assign) CGFloat deltaTime;
@property (nonatomic, assign) double sampleRate;
@property (copy) Signal signal;
@property (nonatomic, strong) AVAudioSourceNode *sourceNode;

@end

@implementation Synth

+ (instancetype)shared {
    static Synth *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[Synth alloc] init];
    });
    
    return shared;
}

- (instancetype)initWithSignal:(Signal)signal {
    self = [super init];
    if (self) {
        _signal = signal;
        _audioEngine = [[AVAudioEngine alloc] init];
        
        AVAudioMixerNode *mainMixer = _audioEngine.mainMixerNode;
        AVAudioOutputNode *outputNode = _audioEngine.outputNode;
        AVAudioFormat *format = [outputNode inputFormatForBus:0];
        
        _sampleRate = format.sampleRate;
        _deltaTime = 1 / (CGFloat)_sampleRate;
        
        AVAudioFormat *inputFormat = [[AVAudioFormat alloc] initWithCommonFormat:format.commonFormat
                                                                      sampleRate:_sampleRate
                                                                        channels:1
                                                                     interleaved:format.isInterleaved];
        [_audioEngine attachNode:self.sourceNode];
        [_audioEngine connect:self.sourceNode to:mainMixer format:inputFormat];
        [_audioEngine connect:mainMixer to:outputNode format:nil];
        mainMixer.outputVolume = 0;
        
        NSError *error;
        [_audioEngine startAndReturnError:&error];
        if (error) {
            NSLog(@"Could not start audioEngine: %@", error.localizedDescription);
        }
    }
    return self;
}

- (instancetype)init {
    return [self initWithSignal:Oscillator.sine];
}

@synthesize sourceNode = _sourceNode;
- (AVAudioSourceNode *)sourceNode {
    if (!_sourceNode) {
        _sourceNode = [[AVAudioSourceNode alloc] initWithRenderBlock:^OSStatus(BOOL * _Nonnull isSilence, const AudioTimeStamp * _Nonnull timestamp, AVAudioFrameCount frameCount, AudioBufferList * _Nonnull outputData) {
        
            for (int frame = 0; frame < frameCount; frame++) {
                CGFloat sampleVal = self.signal(self.time);
                self.time += self.deltaTime;
                
                for (int channel = 0; channel < outputData->mNumberBuffers; channel++) {
                    Float32 *bufferData = (Float32 *)outputData->mBuffers[channel].mData;
                    bufferData[frame] = sampleVal;
                }
            }
            
            return 0;
        }];
    }
    return _sourceNode;
}

- (CGFloat)volume {
    return self.audioEngine.mainMixerNode.outputVolume;
}

- (void)setVolume:(CGFloat)volume {
    self.audioEngine.mainMixerNode.outputVolume = volume;
}

- (void)setWaveformTo:(Signal)signal {
    self.signal = signal;
}

@end
