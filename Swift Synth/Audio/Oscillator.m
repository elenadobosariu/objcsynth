//
//  Oscillator.m
//  Swift Synth
//
//  Created by Elena Dobosariu on 5/27/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Oscillator.h"

@implementation Oscillator

// Amplitude
static CGFloat _amplitude;
+ (CGFloat)amplitude { return _amplitude; }
+ (void)setAmplitude:(CGFloat)val { _amplitude = val; }

// Frequency
static CGFloat _frequency;
+ (CGFloat)frequency { return _frequency; }
+ (void)setFrequency:(CGFloat)val { _frequency = val; }

// Waveforms
Signal const _sine = ^CGFloat(CGFloat time)  {
    return Oscillator.amplitude * sin(2.0 * M_PI * Oscillator.frequency * time);
};
+ (nonnull Signal)sine { return _sine; }

Signal const _whiteNoise = ^CGFloat(CGFloat time) {
    return Oscillator.amplitude * ((CGFloat)rand() / RAND_MAX) * 2 - 1;
};
+ (nonnull CGFloat (^)(CGFloat))whiteNoise { return _whiteNoise; }

Signal const _square = ^CGFloat(CGFloat time)  {
    double period = (double)(1.0f / Oscillator.frequency);
    double currentTime = fmod((double)time, period);

    if (currentTime / period < 0.5) {
        return Oscillator.amplitude;
    }
    return -1.0f * Oscillator.amplitude;
};
+ (nonnull Signal)square { return _square; }

Signal const _sawtooth = ^CGFloat(CGFloat time)  {
    CGFloat period = 1.0f / Oscillator.frequency;
    CGFloat currentTime = fmodf(time, period);

    return Oscillator.amplitude * ((currentTime / period) * 2 - 1.0f);
};
+ (nonnull Signal)sawtooth { return _sawtooth; }

Signal const _triangle = ^CGFloat(CGFloat time)  {
    double period = (double)(1.0f / Oscillator.frequency);
    double currentTime = fmod((double)time, period);
    
    double value = currentTime / period;
    double result = 0.0;
    if (value < 0.25) {
        result = value * 4;
    } else if (value < 0.75) {
        result = 2.0 - value * 4.0;
    } else {
        result = value * 4 - 4;
    }
    return Oscillator.amplitude * (CGFloat)result;
    
};
+ (nonnull Signal)triangle { return _triangle; }

@end
