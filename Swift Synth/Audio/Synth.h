//
//  Synth.h
//  Swift Synth
//
//  Created by Elena Dobosariu on 5/27/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Oscillator.h"

@interface Synth : NSObject

@property (nonatomic, assign) CGFloat volume;

+ (instancetype)shared;
- (void)setWaveformTo:(Signal)signal;

@end
